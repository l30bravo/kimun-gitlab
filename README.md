# kimun-gitlab

Apuntes sobre gitlab y gitlab ci

# CI / CD
![ci cd](imgs/pipeline.png)


## CI Advantages
*  Errors are detected early in the development process (for this has happening, you need to have unit test, integration test, code inspections, among other things in the pipeline.)
* Reduces integration problems
* Allow developers to work faster

## CD Advantages
* Ensures that every change is releasable by testing that it can be deployed
* Reduces risk of a new deployment
* Delivers values mush faster

# Pipeline life cycle
1. Build
2. Test
3.

# Envirorment
* [Envirorment docu](https://gitlab02.uchile.cl/help/ci/environments/index.md)
* [Gitlab CI Pipeline, Artifacts and Environments](https://www.youtube.com/watch?v=PCKDICEe10s)
Example of setting dynamic environment URLs
The following example shows a Review App that creates a new environment
per merge request. The review job is triggered by every push, and
creates or updates an environment named review/your-branch-name.
The environment URL is set to $DYNAMIC_ENVIRONMENT_URL:
```
review:
  script:
    - DYNAMIC_ENVIRONMENT_URL=$(deploy-script)                                 # In script, get the environment URL.
    - echo "DYNAMIC_ENVIRONMENT_URL=$DYNAMIC_ENVIRONMENT_URL" >> deploy.env    # Add the value to a dotenv file.
  artifacts:
    reports:
      dotenv: deploy.env                                                       # Report back dotenv file to rails.
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    url: $DYNAMIC_ENVIRONMENT_URL                                              # and set the variable produced in script to `environment:url`
    on_stop: stop_review

stop_review:
  script:
    - ./teardown-environment
  when: manual
  environment:
    name: review/$CI_COMMIT_REF_SLUG
    action: stop
```

## Configure dynamic environment
* [Configure dynamic environment](https://docs.gitlab.com/ee/ci/environments/index.html#configuring-dynamic-environments)


# Variables
 * [Predefined variables reference ](git@gitlab.com:l30bravo/kimun-gitlab.git)

# Leavels
* `artifacts` - this is used to share with de next job the output of the previous job, For example:
```
build the car:
  stage: build
  script:
    - mkdir build
    - cd build
    - touch cat.txt
    - echo "chassis" > car.txt
    - echo "engine" > car.txt
    - echo "wheels" > car.txt
  artifacts:
    paths:
      - build/
```
* `when` - [when](https://docs.gitlab.com/ee/ci/yaml/#when), Use when to implement jobs that run in case of failure or despite the failure.
* `allow_failure` - [allow_failure](https://docs.gitlab.com/ee/ci/yaml/#allow_failure), Use allow_failure when you want to let a job fail without impacting the rest of the CI suite. The default value is false, except for manual jobs that use the when: manual syntax.
* `Only and except` - [only and except basic](https://docs.gitlab.com/ee/ci/yaml/#onlyexcept-basic), only and except are two keywords that determine when to add jobs to pipelines, `only` defines the names of branches and tags the job runs for; `except` defines the names of branches and tags the job does not run for.
```
only:
  - master
  - merge_requests
```
* `bedore_script` and `after_script` - [bedore_script and after_script](https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script)

# Protected Branches
* Settings -> Repository -> Protected Branches
![Protected branches](imgs/pbranches.png)
Here you can apply the branch policies that allow you to comply with the gitflow branching model, do not commit directly to release branches, force to generate Pull Requests that must be analyzed (visual code inspection) and approved by the maintainers of branches, this control will help the master code to be a real potential release and have stability

* [gitflow workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

# Merge Request
* Settings -> general -> Merge requests; Pipelines must succeed must be on

# API gitlab
*

# Gitlab Runners
* [Runner Doc](https://docs.gitlab.com/runner/)
* [API - Runner Doc](https://docs.gitlab.com/ee/api/runners.html)
* [API - Protected Branches](https://docs.gitlab.com/ee/api/protected_branches.html)
* [Installing Runner using the Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)
* [Group Runners](https://docs.gitlab.com/ee/ci/runners/#group-runners)
* [How to configure your own Gitlab CI Runner - Video](https://www.youtube.com/watch?v=G8ZONHOTAQk)
* [Advance Configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)


## Installing in Linux
```
wget https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
sudo dpkg -i gitlab-runner_amd64.deb
gitlab-runner register
sudo gitlab-runner install --working-directory /home/leo/git/uchile/runner --user leo
```

```
sudo gitlab-runner uninstall
```

## Start and run gitlab-runner
```
sudo gitlab-runner start
gitlab-runner --debug run
```
## Other gitlab-runners commands
* `gitlab-runner --help`
* `gitlab-runner list`
* `gitlab-runner unregister --all-runners` - remove the runner from Repository

* [how-set-gitlabs-cicd-runner-kubernetes](https://www.corevaluetech.com/blog/how-set-gitlabs-cicd-runner-kubernetes) - `/etc/gitlab-runner/config.toml`

# Links
* [Gitlab CI Course Notes - edit March 2020](https://buildmedia.readthedocs.org/media/pdf/gitlab-ci-course-notes/latest/gitlab-ci-course-notes.pdf)
* [Gitlab CI code_quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)
* [Gitlab CI code_intelligence](https://docs.gitlab.com/ee/user/project/code_intelligence.html)
